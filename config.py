# Define the application directory
import os
basedir = os.path.abspath(os.path.dirname(__file__))

SECRET_KEY = os.environ.get('SECRET_KEY') or 'traidatnaylacuachungminh'
DEBUG = True
SQLALCHEMY_DATABASE_URI = \
    'sqlite:///' + os.path.join(basedir, 'data_dev.sqlite.db')
SQLALCHEMY_TRACK_MODIFICATIONS = True
