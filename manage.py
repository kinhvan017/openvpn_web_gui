#!/usr/bin/env python
from app import app
from flask.ext.script import Manager, Server
from app import db

manager = Manager(app)
manager.add_command("runserver", Server(host="0.0.0.0", port=9000))


@manager.command
def createdb():
        db.create_all()


@manager.command
def dropdb():
        db.drop_all()

if __name__ == '__main__':
    manager.run()
