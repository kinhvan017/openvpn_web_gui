from logging import DEBUG

from flask import Flask, redirect, url_for

from .models import db

app = Flask(__name__)
app.logger.setLevel(DEBUG)
app.config.from_object('config')
db.init_app(app)

# Register Blueprint "/home"
from app.home import home
from app.auth import auth

app.register_blueprint(home, url_prefix='/home')
app.register_blueprint(auth, url_prefix='/auth/')


@app.route('/')
def index():
    return redirect(url_for('home.index'))
