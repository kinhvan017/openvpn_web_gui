from flask.ext.wtf import Form
from wtforms import StringField, PasswordField, BooleanField, SubmitField
from wtforms.validators import Required, Length, Email, EqualTo
from wtforms import ValidationError
from .models import User


class LoginForm(Form):
    email = StringField('Email', validators=[Required(), Length(1, 64),
                                             Email()])
    password = PasswordField('Password', validators=[Required()])
    remember_me = BooleanField('Keep me logged in')
    submit = SubmitField('Log In')


class RegistrationForm(Form):
    inputEmail = StringField('inputEmail',
                             validators=[Required(), Length(1, 64), Email()])
    inputPassword = PasswordField(
        'inputPassword',
        validators=[
            Required(),
            EqualTo('inputPassword2', message="Password must match")
        ]
    )
    inputPassword2 = PasswordField('inputPassword', validators=[Required()])
    submit = SubmitField('Register')

    def validate_inputEmail(self, field):
        if User.query.filter_by(username=field.data).first():
            raise ValidationError('Email have already registered.')
