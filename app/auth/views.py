from flask import (
    request, render_template,
    flash, redirect, url_for
)
from app import db
from app.forms import RegistrationForm

from app.models import User
from . import auth


@auth.route('signin')
def signin():
    return render_template('signin.html')


@auth.route('signup', methods=['GET', 'POST'])
def signup():
    form = RegistrationForm()
    if request.method == 'POST' and form.validate_on_submit():
        user = User(username=form.inputEmail.data,
                    password=form.inputPassword.data)
        db.session.add(user)
        db.session.commit()
        flash('You were successfully registered')
        return redirect(url_for('auth.signin'))
    if form.errors:
        for field, errors in form.errors.items():
            for error in errors:
                flash("%s" % error)
    return render_template('signup.html', form=form)
