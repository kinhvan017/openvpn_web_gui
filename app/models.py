from datetime import datetime

from werkzeug.security import generate_password_hash, check_password_hash
from flask.ext.sqlalchemy import SQLAlchemy

db = SQLAlchemy()


class User(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    username = db.Column(db.String(20), unique=True, index=True)
    password_hash = db.Column(db.String(128))
    create_at = db.Column(db.DateTime(), default=datetime.utcnow)

    @property
    def password(self):
        raise AttributeError('password is not a readable attribute')

    @password.setter
    def password(self, password):
        self.password_hash = generate_password_hash(password)

    def verify_password(self, password):
        return check_password_hash(self.password_hash, password)

    def __repr__(self):
        return '<User %r>' % self.username


class VpnInfo(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    secret_key = db.Column(db.String, nullable=False)
    active = db.Column(db.Boolean, unique=False, default=True)

    def __repr__(self):
        return '<Secret: %r, is_active: %r>' % (self.secret_key, self.active)
